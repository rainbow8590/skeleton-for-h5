var windowWidth = $(window).width()
var windowHeight = $(window).height()

/* skeleton 写入css */
/* 
* bgColor @params 背景颜色 
*/
SkeletonFun('#fff')
function SkeletonFun(bgColor){
	// 底部全屏颜色
	$('.skeleton-wrap').css({
		width: windowWidth,
		height: windowHeight,
		backgroundColor:bgColor,
		position: 'absolute',
		left:0,
		top:0,
		zIndex: 9998,
		overflow: 'hidden'
	})
	//  查找所有的react
	var allRect = $('.skeleton').find('.skeleton-rect')
	var allRadius = $('.skeleton').find('.skeleton-radius')
	console.log(allRect)
	var str = '';
	for(var i = 0; i < allRect.length; i++){
		str += '<div style="width: '+allRect[i].offsetWidth+'px; height: '+allRect[i].offsetHeight+'px; background-color: rgb(194, 207, 214);position: absolute; left: '+allRect[i].offsetLeft+'px; top: '+allRect[i].offsetTop+'px"></div>'
	}
	for(var i = 0; i < allRadius.length; i++){
		str += '<div style="width: '+allRadius[i].offsetWidth+'px; height: '+allRadius[i].offsetHeight+'px; background-color: rgb(194, 207, 214); border-radius: '+allRadius[i].offsetWidth+'px; position: absolute; left: '+allRadius[i].offsetLeft+'px; top: '+allRadius[i].offsetTop+'px"></div>'
	}
	$('.skeleton-wrap').prepend(str)

	setTimeout(()=>{
		$('.skeleton-wrap').css({display:'none'})
	},1000)
}

